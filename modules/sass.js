/*
 * Dependencias
 */
const
	helpers 	= require('./helpers'),
	argv 		= require('yargs').argv,
	path 		= require('path'),
	mergeStream = require('merge-stream'),
	_ 			= require('lodash'),
	gulp 		= require('gulp'),
	$ 			= require('gulp-load-plugins')()
;

/*
 * Config
 */
const 
	config 	= global.config,
	banner 	= helpers.createComment(config.banner),
	//helper
	getConfNormalize 	= helpers.getConfNormalize,
	swallowError 		= helpers.swallowError
;

/**
 * Replicar los archivos a la ruta definida en dev
 */
gulp.task('sass', function(){
	let confSass = _.isEmpty(global.conf_sass) ? getConfNormalize('scss') : global.conf_sass,
		tasks = new mergeStream();

	global.conf_sass = null;
	
	_.forEach(confSass, function(conf){
		conf.dest = _.castArray(conf.dest);
		
		_.forEach(conf.dest, function(dest){
			tasks.add(
				gulp.src( conf.src, _.get(conf, 'base', {}) )
 				.pipe( $.sourcemaps.init({loadMaps: true}) )
				.pipe( $.size({ showFiles: true }) )
				.pipe( $.sass( _.get(config, 'sass', {}) ) )
    			.on('error', swallowError)
				.on('error', $.util.log)
				.pipe( $.autoprefixer() )
  				.pipe( $.sourcemaps.write('.'))
				.pipe( $.header(banner, { config : config }) )
				.pipe( gulp.dest( dest ) )
				.pipe( $.size({ showFiles: true }) )
				.on('end', () => $.util.log(
					$.util.colors.black.bgGreen( $.util.colors.bold.inverse('SASS') +' '+ $.util.colors.bold('Success') +' converted to css' )
				))
			);
		});
	});

	return tasks;
});