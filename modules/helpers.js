const
	jsonFile 	= require('jsonfile'),
	fs 			= require('fs'),
	path 		= require('path'),
	mm 			= require('micromatch'),
	mergeJSON 	= require('merge-json'),
	_	 		= require('lodash')
;

/**
* Facilita la generación del banner para los archivos generados
* @param  array comments Arreglo con el texto deseado
* @return string El banner ya formateado
*/
function createComment(comments)
{
	let output = `/*\n`;

	comments.forEach((line) => output += `* ${line}\n`);

	return output += `*/\n`;
}

/**
* Valida la existencia de un archivo específico
* @param  string   path  Ruta del archivo que deseamos verificar
* @return boolean        true|false Si existe o no el archivo
*/
function fileExist(path)
{
	var status = true;

	try {
		// Si todo va bien esto NO devolverá nada, así que el status será true
		fs.accessSync(path, fs.F_OK | fs.R_OK);
	} catch(error) {
		// Es necesario cachar el error porque este detiene la ejecución
		status = false;
	}

	return status;
}

function getJson(file){
	return fileExist(file) ? require( path.relative(__dirname, file) ) : {};
}


/**
 * Genera el archivo `gulp.config.json`, es posible pasarle como argumento la ruta del archivo:
 * Así se genera la configuración mezclando el default con el del usuario ubicado en la ruta default:
 * `gul set:config`
 *
 * Así se genera la configuración mezclando el default con el del usaurio ubicado en la ruta que el usuario elija:
 * `gulp set:config --config="../../../my.gulp.config.json"`
 *
 * En caso de que la ruta personalizada sea incorrecta o no sea un json se genera solo el default
 */
function mergeConfig(configPath, configUser, configDefault, configFinal)
{
	// Este archivo siempre estará en el mismo lugar
	let jsonDefault = getJson(configDefault);
	let jsonUser    = {};

	// Si se pasa este `flag` especial quiere decir que el usuario está definiendo una ruta personalizada de su configuración
	if (configPath) {
		if (fileExist(configPath)) {
			try {
				jsonUser = getJson(configPath);
			} catch(err) {
				console.log(`${configPath} no es un archivo JSON válido`);
			}
		} else {
			console.log(`No fue posible localizar el archivo de configuración: ${configPath}`);
		}
	} else {
		jsonUser = getJson(configUser);
	}

	// Obtenemos el JSON final
	let result = mergeJSON.merge(jsonDefault, jsonUser);
	
	// Creamnos nuestra configuración final
	jsonFile.writeFileSync(configFinal, result, { spaces : 2 });
}

/**
 * Formate la configuracio del minificado
 */
function getConfNormalize(on){
	let src 	= global.config[on],
		dest 	= global.config.dest[on],
		minifies = [
			{	
				src: src,
				rename: { suffix: '.min' },
				dest: dest
			}
		];

	if(_.isPlainObject( src )){
		minifies = [];

		_.forEach(src, function(value, key){
			let 
				minify = {
					src: value
				},
				name = key;

			if(key.indexOf('/') >= 0){
				name = path.basename(spKey);
				dest = _.replace(key, name, '');
			}

			minify.dest = dest;

			if(_.isArray(value)){
				minify.concat = name;
			}
			else if(_.isString(value)){
				if(value.indexOf('*') >= 0){
					minify.sourcemaps = true;
					minify.concat = name;
				}
				else{
					minify.rename = name;
				}
			}

			minifies.push(minify);
		});
	}

	return minifies;
}

/**
 * Filtra archivos de una configuracion formateada
 */
function filterFile(doThis, pathFile){
	return _(JSON.parse(JSON.stringify(doThis)))
		.filter(function(o){
			var src = _.get(o, 'src', []);
			//src siempre en array
			src = _.castArray(src);
			//encuentra la primera coincidencia entre el src de la configuracion y el archivo
			var srcMatch = _.find(src, (s) => ( s.indexOf('!') < 0 && mm.isMatch(pathFile, path.resolve(s)) ));

			//sin archivos
			srcMatch = path.basename(srcMatch).indexOf('.') > 0 ? _.replace(srcMatch, path.basename(srcMatch), '') : srcMatch;
			// //sin expresiones regulares
			srcMatch = _(srcMatch)
						.split('/')
						.filter( (s) => (s.indexOf('*') < 0) )
						.join('/');
			//obtener la base a partir del src match
			o.base = { base: path.resolve(srcMatch) };

			return !_.isUndefined(srcMatch);
		})
		.map(function(o){
			//cuando se trata de un array se modifica a la ruta del archivo
			if(_.isArray(o.src)) o.src = [pathFile];
			return o;
		})
		.value();
}

function getPathsFromObject(tree){
    var leaves = [];
    var walk = function(obj, path){
        path = path || "";
        for(var n in obj){
            if (obj.hasOwnProperty(n)) {
                if(typeof obj[n] === "object" || obj[n] instanceof Array) {
                    walk(obj[n],path + (path.length > 0 ? "." : "") + n);
                } else {
                    leaves.push(path + (path.length > 0 ? "." : "") + n);
                }
            }
        }
    }
    walk(tree);
    return leaves;
}

function swallowError (error) { 
  this.emit('end');
}

module.exports = {
	createComment: 		createComment,
	fileExist: 			fileExist,
	mergeConfig: 		mergeConfig,
	filterFile:  		filterFile,
	getConfNormalize: 	getConfNormalize,
	getJson: 			getJson,
	getPathsFromObject: getPathsFromObject,
	swallowError:		swallowError
}