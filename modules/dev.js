/*
 * Dependencias
 */
const
	helpers 	= require('./helpers'),
	argv 		= require('yargs').argv,
	path 		= require('path'),
	mergeStream = require('merge-stream'),
	_ 			= require('lodash'),
	gulp 		= require('gulp'),
	$ 			= require('gulp-load-plugins')()
;

/*
 * Config
 */
const 
	config = global.config,
	//helpers
	getConfNormalize 	= helpers.getConfNormalize,
	swallowError 		= helpers.swallowError
;

/**
 * Replicar los archivos a la ruta definida en dev
 */
gulp.task('dev', function(){
	let confDev = _.isEmpty(global.conf_dev) ? getConfNormalize('dev') : global.conf_dev,
		tasks = new mergeStream();

	global.conf_dev = null;
	
	_.forEach(confDev, function(conf){
		conf.dest = _.castArray(conf.dest);

		_.forEach(conf.dest, function(dest){
			if(dest.indexOf('!') < 0){
				tasks.add(
					gulp.src( conf.src, _.get(conf, 'base', {}) )
					.pipe( gulp.dest( dest ) )
					.on('error', swallowError)
					.on('error', $.util.log)
					.on('end', () => $.util.log(
						$.util.colors.black.bgGreen( $.util.colors.bold.inverse('DEV') +' '+ $.util.colors.bold('Success') +' moved to dev' )
					))
				);
			}
		});

	});
	return tasks;
});