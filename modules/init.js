
const 
	gulp 		= require('gulp'),
	runSequence = require('run-sequence')
;
/**
 * Inicializar todas la tareas
 */
gulp.task('init', function(){
	runSequence('config', 'yaml', 'dev', 'sass', 'minifyCss', 'minifyJs', 'watch');
});
