'use strict';

/*
 * Dependencias
 */
const
	helpers 	= require('./helpers'),
	argv 		= require('yargs').argv,
	mergeStream = require('merge-stream'),
	runSequence = require('run-sequence'),
	_ 			= require('lodash'),
	gulp 		= require('gulp'),
	$ 			= require('gulp-load-plugins')()
;

/*
 * Config
 */
const config = global.config;

/*
 * Helpers
 */
const 
	getConfNormalize 	= helpers.getConfNormalize,
	banner 				= helpers.createComment(config.banner),
	swallowError 		= helpers.swallowError
;

/**
 * Minificar
 */
gulp.task('minify', function(){
	runSequence('minifyCss', 'minifyJs');
});

/**
 * Minificar css
 */
gulp.task('minifyCss', function(){
	let confMin = _.isEmpty(global.conf_min_css) ? getConfNormalize('minify_css') : global.conf_min_css,
		tasks = new mergeStream();

	global.conf_min_css = null;

	//recorre la configuracion para minificar css
	_.forEach(confMin, function(conf){
		conf.dest = _.castArray(conf.dest);
		
		_.forEach(conf.dest, function(dest){
			tasks.add(
				gulp.src( conf.src, _.get(conf, 'base', {}) )
				.pipe( $.size({ showFiles: true }) )
				.pipe( _.has(conf, 'sourcemaps') 	? 	$.sourcemaps.init() 	: $.util.noop() )
				.pipe( _.has(conf, 'concat') 		? 	$.concat(conf.concat) : $.util.noop() )
				.pipe( _.has(conf, 'sourcemaps') 	? 	$.sourcemaps.write() 	: $.util.noop() )
				.pipe( _.has(conf, 'rename') 		? 	$.rename(conf.rename) : $.util.noop() )
				.pipe( $.cssnano( _.get(config, 'cssnano', {}) ) )
    			.on('error', swallowError)
				.on('error', $.util.log)
				.pipe( $.header(banner, { config : config }) )
				.pipe( _.has(conf, 'dest') 		? 	gulp.dest(dest) 	: $.util.noop() )
				.pipe( $.size({ showFiles : true }) )
				.on('end', () => $.util.log(
					$.util.colors.black.bgGreen( $.util.colors.bold.inverse('CSS') +' '+ $.util.colors.bold('Success') +' minified' )
				))
			);
		});
	});

	return tasks;
});

/**
 * Minificar js
 */
gulp.task('minifyJs', function(){
	let confMin = _.isEmpty(global.conf_min_js) ? getConfNormalize('minify_js') : global.conf_min_js,
		tasks = new mergeStream();

	global.conf_min_js = null;

	//recorre la configuracion para minificar js
	_.forEach(confMin, function(conf){
		conf.dest = _.castArray(conf.dest);
		
		_.forEach(conf.dest, function(dest){
			tasks.add(
				gulp.src( conf.src, _.get(conf, 'base', {}) )
				.pipe( $.size({ showFiles: true }) )
				.pipe( _.has(conf, 'sourcemaps') 	? 	$.sourcemaps.init() 	: $.util.noop() )
				.pipe( _.has(conf, 'concat') 		? 	$.concat(conf.concat) : $.util.noop() )
				.pipe( _.has(conf, 'sourcemaps') 	? 	$.sourcemaps.write() 	: $.util.noop() )
				.pipe( _.has(conf, 'rename') 		? 	$.rename(conf.rename) : $.util.noop() )
				.pipe( $.uglify( _.get(config, 'uglify', {}) ) )
    			.on('error', swallowError)
				.on('error', $.util.log)
				.pipe( $.header(banner, { config : config }) )
				.pipe( _.has(conf, 'dest') 		? 	gulp.dest(dest) 	: $.util.noop() )
				.pipe( $.size({ showFiles: true }) )
				.on('end', () => $.util.log(
					$.util.colors.black.bgGreen( $.util.colors.bold.inverse('JS') +' '+ $.util.colors.bold('Success') +' minified' )
				))
			);
		});
	});

	return tasks;
});
