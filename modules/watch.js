/*
 * Dependencias
 */
const
	helpers 	= require('./helpers'),
	argv 		= require('yargs').argv,
	path 		= require('path'),
	mergeStream = require('merge-stream'),
	mm 			= require('micromatch'),
	runSequence = require('run-sequence')
	_ 			= require('lodash'),
	gulp 		= require('gulp'),
	$ 			= require('gulp-load-plugins')()
;

/*
 * Helpers
 */
const 
	filterFile = helpers.filterFile,
	getConfNormalize = helpers.getConfNormalize
;

/*
 * Config
 */
const config = global.config;

/**
 * Observar cambios en los archivos y hacer las tareas correspondiente
 */
gulp.task('watch', function() {
	let 
		confDev 		= getConfNormalize('dev'),
		confSass 		= getConfNormalize('scss'),
		confCss 		= getConfNormalize('minify_css'),
		confJs 			= getConfNormalize('minify_js'),
		
		//Se obtiene el src de dev
		filesDev 		= _(confDev)
						.mapValues('src')
						.toArray()
						.flattenDeep()
						.uniq()
						.value(),
		//Se obtiene el src de sass
		fileSass 		= _(confSass)
						.mapValues('src')
						.toArray()
						.flattenDeep()
						.uniq()
						.value(),
		//Se obtiene el src de minify_css
		filesCss 		= _(confCss)
						.mapValues('src')
						.toArray()
						.flattenDeep()
						.uniq()
						.value(),
		//Se obtiene el src de minify_js
		fileJs 			= _(confJs)
						.mapValues('src')
						.toArray()
						.flattenDeep()
						.uniq()
						.value();
	// confDestDev 	= _(confDev).map(function(conf){
	// 			//Invierte src por dest
	// 				let dest = conf.dest,
	// 					fnSrc  = (s) => _(s)
	// 							.split('/')
	// 							.filter( (s) => (s.indexOf('*') < 0) )
	// 							.join('/'),
	// 					fnDest = (s) => ( path.basename(s).indexOf('.') < 0  ? _.trimEnd(s, '/') + "/**/*" : s );

	// 				dest = _.castArray(dest);
	// 				dest =  _.map(dest, (d) => fnDest(d));
					
	// 				conf.dest = _.map(conf.src, fnSrc);
	// 				conf.src = dest;
	// 				return conf;
	// 			})
	// 			.flattenDeep()
	// 			.value(),
	// //Se obtiene el dest de dev
	// filesDestDev 	= _(confDestDev)
	// 				.mapValues('src')
	// 				.toArray()
	// 				.flattenDeep()
	// 				.uniq()
	// 				.value()
	// ;

    gulp.watch(filesDev, ['dev']).on('change', function(event) {
		global.conf_dev = filterFile(confDev, event.path);

   		$.util.log(
   			$.util.colors.black.bgCyan( $.util.colors.bold.inverse('DEV ORG') +' '+ $.util.colors.bold(event.type) +' '+ event.path )
   		);
	});
	// gulp.watch(filesDestDev, ['dev']).on('change', function(event) {
	// 	global.conf_dev = filterFile(confDestDev, event.path);

   	// 	$.util.log(
   	// 		$.util.colors.black.bgCyan( $.util.colors.bold.inverse('DEV CLONE') +' '+ $.util.colors.bold(event.type) +' '+ event.path )
   	// 	);
	// });
    gulp.watch(filesCss, ['minifyCss']).on('change', function(event) {
		global.conf_min_css = filterFile(confCss, event.path);

   		$.util.log(
   			$.util.colors.black.bgCyan( $.util.colors.bold.inverse('CSS') +' '+ $.util.colors.bold(event.type) +' '+ event.path )
   		);
	});
    gulp.watch(fileJs, ['minifyJs']).on('change', function(event) {
		global.conf_min_js = filterFile(confJs, event.path);

   		$.util.log(
   			$.util.colors.black.bgCyan( $.util.colors.bold.inverse('JS') +' '+ $.util.colors.bold(event.type) +' '+ event.path )
   		);
	});

   	gulp.watch(fileSass, ['sass']).on('change', function(event) {
   		$.util.log(
   			$.util.colors.black.bgCyan( $.util.colors.bold.inverse('SASS') +' '+ $.util.colors.bold(event.type) +' '+ event.path )
   		);
	});
	$.util.log(
		$.util.colors.black.bgWhite.bold( '>>>> WATCHING NOW <<<<' )
	);
});