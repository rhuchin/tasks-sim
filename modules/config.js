'use strict';
/*
 * Dependencias
 */
const
	helpers 		= require('./helpers'),
	argv 			= require('yargs').argv,
	dateFormat 		= require('dateformat'),
	path 			= require('path'),
	gulp 			= require('gulp'),
	_ 	 			= require('lodash')
;

/**
 * Definimos las rutas de nuestros archivos de configuración, estos son 3:
 * gulp.config.json : Al inicio no existe, se genera automáticamente la primera vez, es una mezcla de los otros dos configs
 * gulp.config.defaul.json : Es nuestra configuración inicial, se sobre escribe con el config del user
 * gulp.config.user.json : Es nuestra configuración personalizada, este debe colocarse fuera de la carpeta del proyecto
 */	
const
	configName   	= `gulp.config`,
	configUser   	= `../${configName}.user.json`,
	configDefault	= `./${configName}.default.json`,
	configFinal  	= `./${configName}.json`
;

/** 
 * Helpers
 */
const
	fileExist 		= helpers.fileExist,
	mergeConfig 	= helpers.mergeConfig,
	createComment 	= helpers.createComment
;

// Se ejecuta automáticamente la primera vez, si se desea volver a mezclar será necesario correr la tarea específica
if (!fileExist(configFinal)) { 
	mergeConfig(argv.config, configUser, configDefault, configFinal);
}

global.config = require(path.relative(__dirname, configFinal));

// Añadimos y/o sobreescribimos información del gulp.config.json, algunos datos son del config.json
global.config.datetime = dateFormat(new Date(), 'dddd, mmmm dS, yyyy, h:MM:ss TT');
global.config.date 	= dateFormat(new Date(), 'yyyy');

/**
 * Normalizar la configuración
 */
let 
	fnNormalizeSrc = function(src, ext){
		src = _.trimEnd(src, '/');
		//si es una carpeta sin expresion regular
		if(path.basename(src).indexOf('.') < 0 && src.indexOf('/**/*') < 0){
			//se añade la expesion (todas las carpetas y archivos)
			src += src.indexOf('/**') >= 0 ? "/*"+ext : "/**/*"+ext;
		}
		return src;
	},
	fnNormalizeThis = function(arrConf){
		//se recorre las opciones de la configuración a normalizar
		_.forEach(arrConf, function(k){
			//extension
			let ext = _.isArray(k) ? k[1] : "";
			//opcion real
			k = _.isArray(k) ? k[0] : k;
			//obtener el valor de la opcion especificada
			let val = _.get( global.config, k, []),
				//funcion para normalizar en forma de array
				fnSrc = (src) => _(src).castArray().map((s) => fnNormalizeSrc(s, ext)).value();
			//segun el tipo del valor se formatea para despues normalizar las src
			val = _.isPlainObject(val) ? _.mapValues(val, fnSrc ) : fnSrc(val);
			//se asigna el valor ya normalizado
			_.set(global.config, k , val);
		});
	}
;
//Se normaliza los src de las siguientes opciones
fnNormalizeThis([['scss', '.scss'], ['minify_js', '.js'], ['minify_css', '.css'], 'dev']);

/**
* Regenera el archivo de configuración
*/
gulp.task('config', () => mergeConfig(argv.config, configUser, configDefault, configFinal));