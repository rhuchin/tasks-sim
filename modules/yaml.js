const 
	helpers 	= require('./helpers'),
	path 		= require('path'),
    yaml        = require('node-yaml'),
	mergeStream = require('merge-stream'),
	_ 	        = require('lodash'),
	gulp        = require('gulp'),
	$ 	        = require('gulp-load-plugins')()
;

/*
 * Config
 */
const 
    config = global.config
    getPathsFromObject  = helpers.getPathsFromObject
;

/**
 * Formatea el archivo .yml
 */
gulp.task('yaml', function(){
    let tasks = new mergeStream();

    tasks.add(
        gulp.src(config.yml)
        .pipe($.data(function(file, cb){
            // throughput the stream in case of bad/absent file data
            if (file.isNull()) return cb( null, file );
            if (file.isStream()) return cb( new Error("Streaming not supported") );

            // try and convert yaml to json
            try {
                let json    = yaml.parse(String(file.contents.toString('utf8'))),
                    paths   = getPathsFromObject(config.formatter_yml);
                  
                _.forEach(paths, function(path){
                    let val = _.get(config.formatter_yml, path);

                    if(_.isString(val)){
                        val = _.template(val)({ config: config });
                    }

                    _.set(json, path, val);
                });

                yaml.writeSync(path.resolve(config.yml), json, 'utf8');

            } catch(e) {
                console.log(e);
            }

            cb();
        }))
    );

	return tasks;
});
