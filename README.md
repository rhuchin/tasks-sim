# Framework Tasks Simetrical
------------------------------
Esta es una librería que agilizan algunas tareas como minificar archivos, versionar el sistema y agregar la leyenda de copyright en el encabezado de los archivos Front(css, js)

# Requisitos
 - Node v6.8 en adelante
 - Nmp v3.10 en adelante
 - Gulp v3.9

# Instalacion
 - Descargar el git dentro de tu proyecto

``` console
$ cd MiProyecto
$ sudo git clone https://<username>@bitbucket.org/simetrical-ondemand/tasks-sim.git
```

 - Entrar a la terminal dentro de la carpeta `tasks-sim` y ejecuta lo siguiente:

``` console
$ cd tasks-sim
$ sudo npm install
```

# Estructura del proyecto
```
    MiProyecto
    |--- tasks-sim
    |   |--- modules
    |   |   '--- [...]
    |   |--- node_modules
    |   |   '--- [...]
    |   |--- gulp.config.default.json
    |   |--- gulp.config.json
    |   |--- gulpfile.js
    |   |--- package.json
    |   '--- [...]
    |--- source (Recomendado)
    |   |--- css
    |   |   '--- [...]
    |   |--- js
    |   |   '--- [...]
    |   |--- scss
    |   |   '--- [...]
    |   '-- [...]
    '-- [...]
```
    
# Configuracion
Para empezar a trabajar con las tareas gulp, es necesario comprender su configuración. La configuración depende de un archivo en especial `MiProyecto/tasks-sim/gulp.config.json`, que se forma a partir de dos archivos de configuración(con la cual hace un merge entre ambos). 

El primero es `MiProyecto/tasks-sim/gulp.config.default.json`, este tiene la estructura de configuración básica por default (no se recomienda modificar).

El otro es `MiProyecto/gulp.config.user.json`, este será nuestras configuración deseada bajo nuestro criterio(es necesario agregarlo para personalizar la configuración).

### Estructura de la configuración ###
La estructura es la siguiente:
``` json
{
    "name": "Simetrical",
    "author": "",
    "email": "",
    "version": "1.0",
    "description": "",
    "banner": [
        "Este archivo es parte del proyecto <%= config.name %> v<%= config.version %>",
        "<%= config.description %>",
        "",
        "Este software es propiedad de Simetrical, S.A. de C.V.,",
        "no es un software libre y no puede ser redistribuído y/o",
        "modificado bajo ningún término que no sea expresamente",
        "autorizado por el propietario.",
        "",
        "@copyright Copyright (c); <%= config.date %> Simetrical, S.A. de C.V.",
        "@link http://www.simetrical.com",
        "@author <%= config.author %> <<%= config.email %>>",
        "@last_modified <%= config.datetime %>",
        "",
        "@filesource"
    ],
    "yml": "../app/config/config.yml",
    "formatter_yml": {
        "twig": {
            "globals": {
                "sim_template": {
                    "app": {
                        "title": "<%= config.name %>",
                        "version": "v<%= config.version %>"
                    }
                }
            }
        }
    },
    "dev": [
        "../source/**/*",
        "!../source/scss/**/*"
    ],
    "scss": "../source/scss/**/*.scss",
    "minify_js": "../source/js/**/*.js",
    "minify_css": "../source/css/**/*.css",
    "dest": {
        "dev": "../web/public/dev/",
        "scss": "../source/css",
        "minify_js": "../web/public/js/",
        "minify_css": "../web/public/css/"
    },
    "cssnano": {
        "safe": true
    },
    "uglify": {},
    "sass": {
        "precision": 8
    },
    "extends": ""
}
```
Opción | Tipo | Descripción
------ | -------- | -----------
name | `string` | Nombre del proyecto
author | `string` | Nombre del creador
email | `string` | Correo del creador
version | `string` | Version del proyecto
description | `string` | Descripción del proyecto
banner | `array[string]` | Encabezado de los archivo
yml | `string` | Ruta del archivo yml a editar
formatter_yml | `object` | Formato json a hacer merge con el archivo yml
dev | `string,  array[string],  object{string:string}` | Ruta donde se localizan los ficheros de dev
scss | `string` | Ruta donde se localizan los ficheros scss
minify_js | `string,  array[string],  object{string:string}` | Ruta donde se localizan los ficheros js
minify_css | `string,  array[string],  object{string:string}` | Ruta donde se localizan los ficheros css
dest.dev | `string, array[string]` | Ruta donde se crean los ficheros de dev
dest.scss | `string` | Ruta donde se crean los css a partir de un scss
dest.minify_js | `string` | Ruta donde se crean los js minificados
dest.minify_css | `string` | Ruta donde se crean los css minificados
cssnano | `object` | Configuración del opcion para [cssnano](https://github.com/ben-eb/cssnano)
uglify | `object` | Configuración del option para [gulp-uglify](https://github.com/terinjokes/gulp-uglify)
sass | `object` | Configuración del option para [gulp-sass](https://github.com/sass/node-sass#options)
extends | `string,  array[string],  object{string:string}` | Agrega modulos como extención

# Como usarlo?
### Tareas disponibles
Las tareas se ejecutan con `gulp [tarea]` dentro de `MiProyecto/task-sim` en la terminal.

Tarea | Descripción
------ | -----------
init | Ejecuta todas las siguientes tareas(`config`, `yaml`, `dev`, `sass`, `minify`,  `watch`).
config | Crea el archivo de configuración `MiProyecto/task-sim/gulp.config.json`. Opcionalmente se puede especificar una configuración personalizada con el parámetro `--user`.
yaml | Formatea el archivo yml haciendo merge con lo contenido en `config.formatter_yml`.
dev | Crea las carpetas y archivos por replica en la ruta especificada(`config.dest.dev`).
sass | Crea los css a partir de los scss de la ruta configurada(`config.scss`) y los mueve a la ruta especificada(`config.dest.scss`).
minify | Minifica todos los archivos configurados(`config.minify_js` & `config.minify_css`) y los mueve en la ruta especificada(`config.dest.minify_js` & `config.dest.minify_css`.
watch | Pone en escucha todas los archivos según lo configurado, y cuando se vean afectados se realizan las tareas pertenecientes.

### Agregar tareas
Puedes incluir tareas personalizadas de tus módulos, agregándolos en el apartado de configuración *extends*. 
De la siguiente forma en tu modulo personalizado podrás reutilizar los módulos que contiene la librería(`api`):
```
const api = module.parent.extends;
```

### Importar libreria
De igual forma puedes usar esta librería(`task-sim`) impotandolo en tu módulos de la siguiente forma:
```
const api = require('MiProyecto/tasks-sim/gulpfile');
```

### Modulos de la libreria
Tienes la posibilidad de reutilizar *gulp*(`gulp`), *lodash*(`_`) y *gulp-load-plugins*(`$`), sin necesidad de instalar e importar estos módulos en tu modulo personalizado, con tan solo agregar estas lineas:
```
const
    gulp    = api.gulp,
    _       = api._,
    $       = api.$
;
```

### Obtener la configuracón
También se puede acceder a la configuración de esta libreria con `global.config`.




# Recomendaciones
 - Crear una carpeta `source` en la raíz de tu proyecto. Ahí crea y modifica tus archivos scss, css y js(ambiente front-end).
```
    MiProyecto
    '--- source (Recomendado)
        |--- css
        |   '--- [...]
        |--- js
        |   '--- [...]
        |--- scss
        |   '--- [...]
        '-- [...]
    
```
 - Crear una carpeta `dev` dentro de la carpeta publica de tu proyecto(en symfony2 sería `web/public/dev`) y agregar la ruta al `.gitignore`.
 - Crear un archivo `gulp.config.user.json` en la raíz de tu proyecto
 - Copia la configuración por default de `gulp.config.default.json` y pégalo en tu archivo  de configuración personalizada(`gulp.config.user.json`), ahora configuras las partes que crea convenientes.
 - Agregar la carpeta `tasks-sim` al `.gitignore`

## Suerte!!! ##