'use strict';

const
	path 		= require('path'),
	requireDir 	= require('require-dir'),
	gulp	 	= require('gulp'),
	_			= require('lodash'),
	$ 			= require('gulp-load-plugins')()
;

var	req = requireDir('./modules');
	
const
	fileExist 	= req.helpers.fileExist,
	extds 		= global.config.extends
;

//Se exporta lo necesario
req.gulp 	= gulp;
req._ 		= _;
req.$ 		= $;

//Obtiene y importa los archivos agregado dentro de config.extends
if(_.isString(extds)){
	requireThis(extds);
}
else if(_.isPlainObject(extds)){
	_.forEach(extds, function(val, key){
		requireThis(val, key);
	});
}
else if(_.isArray(extds)){
	_.forEach(extds, function(val, key){
		requireThis(val);
	});
}

//default task
gulp.task('default', ['init']);

//exportar todos
module.exports = req;

/*
 * Valida y importa la ruta especificada
 */
function requireThis(pathReq, keyReq){
	//exportar antes de cada fichero
	module.exports = req;

	let new_req = {};
		
	//Si es un archivo y existe
	if(! _.isEmpty(path.extname(pathReq)) ){
		if( fileExist(pathReq) ){
			let basename = path.basename(pathReq, path.extname(pathReq));

			if(_.isUndefined(keyReq)){
				keyReq = basename;
			}

			new_req[ _.camelCase(keyReq) ] = require( path.dirname(pathReq) +"/"+ basename );
		}
	}
	else{ //si pertenece a una carpeta
		new_req = requireDir(pathReq);
	}

	req = _.defaults(req, new_req);
}

